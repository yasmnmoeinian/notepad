package com.sourcebama.myapplication2.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.sourcebama.myapplication2.model.Notes;

@Database(entities = Notes.class,version = 1,exportSchema = false)
public abstract class NoteDB extends RoomDatabase {


    public  abstract NotesDao notesDao();
    public static final String DATABASE_NAME = "notessky";

    private static NoteDB instance;

    public static NoteDB getInstance(Context context) {
        if (instance == null)
            instance = Room.databaseBuilder(context,NoteDB.class,DATABASE_NAME).allowMainThreadQueries().build();
        return instance;
    }
}
