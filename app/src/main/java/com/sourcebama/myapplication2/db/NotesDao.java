package com.sourcebama.myapplication2.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.sourcebama.myapplication2.model.Notes;

import java.util.List;

@Dao
public interface NotesDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNote(Notes notes);

    @Update
    void updateNote(Notes notes);

    @Delete
    void deleteNote(Notes notes);

    @Query("SELECT *FROM notef")
    List<Notes> getNotes();

    @Query("SELECT *FROM notef WHERE id = :noteId")
    Notes getNotesById(int noteId);

    @Query("DELETE FROM notef WHERE id = :noteId")
    void deleteNotesById(int noteId);

    @Query("SELECT *FROM notef WHERE text = :text")
    List<Notes> searchText(String text);
}
