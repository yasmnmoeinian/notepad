package com.sourcebama.myapplication2.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class NoteUtils {
    public static String FullDataFromLong(long time){
        DateFormat format = new SimpleDateFormat("EEE,dd yyyy 'at' hh:mm aaa", Locale.US);
        return format.format(new Date(time));
    }

    public static String dataFromLong(long time) {
        DateFormat format = new SimpleDateFormat("yyyy EEE,dd", Locale.US);
        return format.format(new Date(time));
    }
}
