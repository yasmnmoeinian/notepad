package com.sourcebama.myapplication2.activitys;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.shreyaspatil.material.navigationview.MaterialNavigationView;
import com.sourcebama.myapplication2.ActivityEnhanced;
import com.sourcebama.myapplication2.App;
import com.sourcebama.myapplication2.R;
import com.sourcebama.myapplication2.adapter.NoteAdapter;
import com.sourcebama.myapplication2.callback.NoteEventListener;
import com.sourcebama.myapplication2.db.NoteDB;
import com.sourcebama.myapplication2.db.NotesDao;
import com.sourcebama.myapplication2.model.Notes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.sourcebama.myapplication2.activitys.NotesActivity.NOTE_EXTRA_KEY;

public class MainActivity extends ActivityEnhanced implements NoteEventListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private MaterialNavigationView navigationView;
    private ImageView ImageMenuNavigation;
    private RecyclerView recyclerView;
    private FloatingActionButton fab,fabNotes,fabList;
    private ArrayList<Notes> notes;
    private NoteAdapter adapter;
    private NotesDao dao;
    private boolean isShowing = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.changeTheme(this);
        App.changeFont(this);
        setContentView(R.layout.activity_main);
        initViews();


    }

    public void initViews() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        dao = NoteDB.getInstance(this).notesDao();

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        ImageMenuNavigation = findViewById(R.id.menu_nav);
        fab = findViewById(R.id.fab);
        fabNotes = findViewById(R.id.fabNotes);
        fabList = findViewById(R.id.fabList);


        recyclerView = findViewById(R.id.notes_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                checkItemNavigation(item);
                return true;
            }
        });

        ImageMenuNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                    drawer.closeDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.RIGHT);
                }
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation fadeIn = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_in);
                Animation fadeOut = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_out);
                RotateAnimation rotate = new RotateAnimation(0, 90, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                rotate.setDuration(1500);
                rotate.setInterpolator(new LinearInterpolator());
                fab.startAnimation(rotate);
                if (isShowing){
                    isShowing = false;
                    fabNotes.setVisibility(View.INVISIBLE);
                    fabList.setVisibility(View.INVISIBLE);
                    fabNotes.startAnimation(fadeOut);
                    fabList.startAnimation(fadeOut);
                }else{
                    isShowing = true;
                    fabNotes.setVisibility(View.VISIBLE);
                    fabList.setVisibility(View.VISIBLE);
                    fabNotes.startAnimation(fadeIn);
                    fabList.startAnimation(fadeIn);

                    fabNotes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onAddNewNote();
                        }
                    });
                }

            }
        });
    }

    private void onAddNewNote() {
        App.startActivity(NotesActivity.class, false);
    }

    private void loadNotes() {
        this.notes = new ArrayList<>();
        List<Notes> list = dao.getNotes();
        Collections.reverse(list);
        notes.addAll(list);
        adapter = new NoteAdapter(notes);
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    @Override
    protected void onResume() {
        super.onResume();
        loadNotes();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(Gravity.RIGHT)) {
            drawer.closeDrawer(Gravity.RIGHT);
        }
    }

    private void checkItemNavigation(MenuItem item) {
        if (item.getItemId() == R.id.nav_share) {
            Toast.makeText(MainActivity.this, "Shared Pressed", Toast.LENGTH_SHORT).show();
            drawer.closeDrawer(Gravity.RIGHT);
        } else if (item.getItemId() == R.id.nav_notes) {
            drawer.closeDrawer(Gravity.RIGHT);
            App.startActivity(NotesActivity.class, false);

        } else if (item.getItemId() == R.id.nav_archive) {
            Toast.makeText(MainActivity.this, "Gallary Pressed", Toast.LENGTH_SHORT).show();
            drawer.closeDrawer(Gravity.RIGHT);
            Intent in = new Intent(MainActivity.this, NotesActivity.class);
            startActivity(in);

        } else if (item.getItemId() == R.id.nav_settings) {
            drawer.closeDrawer(Gravity.RIGHT);
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));

        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onNoteClickListener(Notes notes) {
        Intent edit = new Intent(this, NotesActivity.class);
        edit.putExtra(NOTE_EXTRA_KEY, notes.getId());
        startActivity(edit);
    }

    @Override
    public void onNoteLongClickListener(final Notes notes) { new AlertDialog.Builder(this).setPositiveButton("حذف", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dao.deleteNote(notes);
                loadNotes();
            }
        }).create().show();

    }



}
