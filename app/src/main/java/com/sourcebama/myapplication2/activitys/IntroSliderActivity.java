package com.sourcebama.myapplication2.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.sourcebama.myapplication2.R;
import com.sourcebama.myapplication2.SliderPrefManager;


public class IntroSliderActivity extends AppCompatActivity {


    private ViewPager viewPager;
    private Button btnSkip, btnNext;
    private LinearLayout layoutDots;
    private SliderPagerAdapter pagerAdapter;
    private SliderPrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_slider);
        changeStatusbarColor();
        initViews();
        pagerAdapter = new SliderPagerAdapter();
        viewPager.setAdapter(pagerAdapter);


        prefManager = new SliderPrefManager(this);
        if (!prefManager.startSlider()) {
            launchMainScreen();
        }

        showDots(viewPager.getCurrentItem());

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {

                showDots(position);
                if (position == viewPager.getAdapter().getCount() - 1) {
                    btnSkip.setVisibility(View.GONE);
                    btnNext.setText(R.string.gotit);
                } else {
                    btnSkip.setVisibility(View.VISIBLE);
                    btnNext.setText(R.string.next);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void launchMainScreen() {
        Intent intent = new Intent(IntroSliderActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void initViews() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnSkip = (Button) findViewById(R.id.btnSkip);
        layoutDots = (LinearLayout) findViewById(R.id.layoutDots);
    }

    private void showDots(int pageNumber) {
        TextView[] dots = new TextView[viewPager.getAdapter().getCount()];
        layoutDots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, 35);
            dots[i].setTextColor(ContextCompat.getColor(this
                    , (i == pageNumber ? R.color.activeDots : R.color.inActiveDots)));
            layoutDots.addView(dots[i]);
        }
    }

    private void changeStatusbarColor() {
     if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
         setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
     }
     if (Build.VERSION.SDK_INT >= 19) {
         getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
     }
     if (Build.VERSION.SDK_INT >= 21) {
         Window window = getWindow();
         setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
         getWindow().setStatusBarColor(getResources().getColor(R.color.statusBarColor));
     }
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public void onClick(View view) {

        if (view.getId() == R.id.btnSkip) {
            launchMainScreen();
        } else if (view.getId() == R.id.btnNext) {
            int currentPage = viewPager.getCurrentItem();
            int lastPage = viewPager.getAdapter().getCount() - 1;

            if (currentPage == lastPage) {
                prefManager.setStartSlider(false);
                launchMainScreen();
            } else {
                viewPager.setCurrentItem(currentPage + 1);
            }
        }
    }

    public class SliderPagerAdapter extends PagerAdapter {
        int[] imageIds = {R.drawable.intro_image_1,R.drawable.intro_image_2,R.drawable.intro_image_3,R.drawable.intro_image_4,R.drawable.intro_image_5};
        String[] slideTitle;
        String[] slideDescriptions;
        public SliderPagerAdapter(){
            slideTitle = getResources().getStringArray(R.array.slide_title);
            slideDescriptions = getResources().getStringArray(R.array.slide_descriptions);

        }
        @Override
        public int getCount() {
            return slideTitle.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {

            View view = LayoutInflater.from(IntroSliderActivity.this)
                    .inflate(R.layout.intro_slider, container, false);
            ((ImageView) view.findViewById(R.id.imageIntroSlider)).setImageResource(imageIds[position]);
            ((TextView) view.findViewById(R.id.titleIntroSlider)).setText(slideTitle[position]);
            ((TextView) view.findViewById(R.id.descriptionIntroSlider)).setText(slideDescriptions[position]);
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
