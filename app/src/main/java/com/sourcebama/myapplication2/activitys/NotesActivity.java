package com.sourcebama.myapplication2.activitys;

import android.app.Dialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputLayout;
import com.sourcebama.myapplication2.ActivityEnhanced;
import com.sourcebama.myapplication2.App;
import com.sourcebama.myapplication2.R;
import com.sourcebama.myapplication2.db.NoteDB;
import com.sourcebama.myapplication2.db.NotesDao;
import com.sourcebama.myapplication2.model.Notes;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;


public class NotesActivity extends ActivityEnhanced {

    private Toolbar toolbar;
    private EditText inputNote, noteTitle;
    private NotesDao dao;
    private Notes temp;
    private Dialog dialog;
    public static final String NOTE_EXTRA_KEY = "note_id";
    String colorTag = "white";
    public boolean isShowDialog = false, isShowSearchToolbar = false, hidden = true;
    private TextInputLayout inputLayout;
    private LinearLayout linearSettings, toolbarSearch, mRevealView;
    private Bitmap thumbnail;
    private static final int SELECT_PHOTO = 1;
    private static final int CAPTURE_PHOTO = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.changeTheme(this);
        App.changeFont(this);
        setContentView(R.layout.activity_notes);
        initViews();
        App.changeThemeLinear(linearSettings);

    }

    private void initViews() {
        // TODO: 5/4/2020 set Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        dao = NoteDB.getInstance(this).notesDao();

        mRevealView = findViewById(R.id.reveal_items);
        noteTitle = findViewById(R.id.noteTitle);
        noteTitle.setVisibility(View.VISIBLE);
        toolbarSearch = findViewById(R.id.toolbarSearch);
        inputLayout = findViewById(R.id.layout_notes);
        linearSettings = findViewById(R.id.linearSettings);
        inputNote = findViewById(R.id.inputNote);
        inputNote.requestFocus();


        if (getIntent().getExtras() != null) {
            int id = getIntent().getExtras().getInt(NOTE_EXTRA_KEY, 0);
            temp = dao.getNotesById(id);
            inputNote.setText(temp.getNoteText());
            noteTitle.setText(temp.getNoteTitle());
            App.setBackgroundNote(temp, inputNote, inputLayout);
        }

        noteTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                hideAttachFileLayout();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.imgSaveNote) {
            onSaveNote();
            finish();
        } else if (id == R.id.imgThemeNotes) {
            showDialog(this);
        } else if (id == R.id.imgAttachFileNotes) {
            showAttachFilePopup();
        }
    }

    private void showAttachFilePopup() {
        final Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);

        Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);
        if (hidden) {
            mRevealView.setVisibility(View.VISIBLE);
            mRevealView.startAnimation(slide_up);
            hidden = false;
            hideKeyboard();

        } else {
            mRevealView.startAnimation(slide_down);
            mRevealView.setVisibility(View.GONE);
            hidden = true;
        }
        inputNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!hidden) {
                    mRevealView.setVisibility(View.GONE);
                    mRevealView.startAnimation(slide_down);
                    hidden = true;
                }
            }
        });


    }

    private void onSaveNote() {
        String text = inputNote.getText().toString();
        String title = noteTitle.getText().toString();
        if (!text.isEmpty()) {
            long date = new Date().getTime();
            if (temp == null) {
                temp = new Notes(text, date, colorTag, title);
                dao.insertNote(temp);
            } else {
                temp.setNoteDate(date);
                temp.setNoteText(text);
                if (isShowDialog) {
                    temp.setNoteBack(colorTag);
                }
                temp.setNoteTitle(title);
                dao.updateNote(temp);
            }
        }
    }

    @Override
    public void onBackPressed() {
        final Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);
        if (!hidden) {
            mRevealView.startAnimation(slide_down);
            mRevealView.setVisibility(View.GONE);
            hidden = true;
        }
        if (isShowSearchToolbar) {
            toolbar.setVisibility(View.VISIBLE);
            toolbarSearch.setVisibility(View.GONE);
            isShowSearchToolbar = false;
        } else {
            super.onBackPressed();
            onSaveNote();
            finish();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.notes_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        String text = inputNote.getText().toString();
        if (id == R.id.menu_delete) {
            hideAttachFileLayout();
            if (!text.isEmpty()) {
                if (getIntent().getExtras() != null) {
                    int idDelete = getIntent().getExtras().getInt(NOTE_EXTRA_KEY, 0);
                    dao.deleteNotesById(idDelete);
                    finish();
                }
            }
        } else if (id == R.id.menu_share) {
            hideAttachFileLayout();
            String textShare = temp.getNoteText();
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT, textShare);
            startActivity(share);
        } else if (id == R.id.menu_search) {
            hideAttachFileLayout();
            isShowSearchToolbar = true;
            toolbar.setVisibility(View.GONE);
            toolbarSearch.setVisibility(View.VISIBLE);


            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            final SearchView searchView = findViewById(R.id.searchView);

            SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());
            searchView.setSearchableInfo(searchableInfo);
            searchView.setIconifiedByDefault(false);
            searchView.setFocusable(true);
            searchView.requestFocusFromTouch();


            findViewById(R.id.imgCloseSearch).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toolbar.setVisibility(View.VISIBLE);
                    toolbarSearch.setVisibility(View.GONE);
                    isShowSearchToolbar = false;
                }
            });


            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String search) {
                    String originalText = inputNote.getText().toString();
                    String replaceWhit = "<span style = 'background-color:#EEA257'>" + search.trim() + "</span>";
                    String modified = originalText.replaceAll(search.trim(), replaceWhit);

                    inputNote.setText(Html.fromHtml(modified));
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    hideAttachFileLayout();
                    String originalText = inputNote.getText().toString();
                    String replaceWhit = "<span style = 'background-color:#EEA257'>" + newText.trim() + "</span>";
                    String modified = originalText.replaceAll(newText.trim(), replaceWhit);

                    inputNote.setText(Html.fromHtml(modified));
                    return false;
                }
            });


        }
        if (id == R.id.menu_cancel) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void showDialog(Context context) {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_background_notes);
        dialog.setTitle("تغییر پوسته");
        dialog.show();

    }

    public void changeBackNote(View v) {
        int id = v.getId();
        if (id == R.id.blackBackground) {
            colorTag = "black";
            isShowDialog = true;
            onSaveNote();
            inputLayout.setBackgroundColor(getResources().getColor(R.color.blackBackground));
            inputNote.setBackgroundColor(getResources().getColor(R.color.blackBackground));
            inputNote.setTextColor(getResources().getColor(R.color.colorAccentNight));
            dialog.dismiss();
        } else if (id == R.id.grayBackground) {
            colorTag = "gray";
            isShowDialog = true;
            onSaveNote();
            inputLayout.setBackgroundColor(getResources().getColor(R.color.grayBackground));
            inputNote.setBackgroundColor(getResources().getColor(R.color.grayBackground));
            inputNote.setTextColor(getResources().getColor(R.color.blackBackground));
            dialog.dismiss();
        } else if (id == R.id.pinkBackground) {
            colorTag = "pink";
            isShowDialog = true;
            onSaveNote();
            inputLayout.setBackgroundColor(getResources().getColor(R.color.pinkBackground));
            inputNote.setBackgroundColor(getResources().getColor(R.color.pinkBackground));
            inputNote.setTextColor(getResources().getColor(R.color.blackBackground));
            dialog.dismiss();
        } else if (id == R.id.orangeBackground) {
            colorTag = "orange";
            isShowDialog = true;
            onSaveNote();
            inputLayout.setBackgroundColor(getResources().getColor(R.color.orangeBackground));
            inputNote.setBackgroundColor(getResources().getColor(R.color.orangeBackground));
            inputNote.setTextColor(getResources().getColor(R.color.blackBackground));
            dialog.dismiss();
        } else if (id == R.id.yellowBackground) {
            colorTag = "yellow";
            isShowDialog = true;
            onSaveNote();
            inputLayout.setBackgroundColor(getResources().getColor(R.color.yellowBackground));
            inputNote.setBackgroundColor(getResources().getColor(R.color.yellowBackground));
            inputNote.setTextColor(getResources().getColor(R.color.blackBackground));
            dialog.dismiss();
        } else if (id == R.id.greenBackground) {
            colorTag = "green";
            isShowDialog = true;
            onSaveNote();
            inputLayout.setBackgroundColor(getResources().getColor(R.color.greenBackground));
            inputNote.setBackgroundColor(getResources().getColor(R.color.greenBackground));
            inputNote.setTextColor(getResources().getColor(R.color.blackBackground));
            dialog.dismiss();
        } else if (id == R.id.purpleBackground) {
            colorTag = "purple";
            isShowDialog = true;
            onSaveNote();
            inputLayout.setBackgroundColor(getResources().getColor(R.color.purpleBackground));
            inputNote.setBackgroundColor(getResources().getColor(R.color.purpleBackground));
            inputNote.setTextColor(getResources().getColor(R.color.blackBackground));
            dialog.dismiss();
        } else if (id == R.id.whitekBackground) {
            colorTag = "white";
            isShowDialog = true;
            onSaveNote();
            inputLayout.setBackgroundColor(getResources().getColor(R.color.white));
            inputNote.setBackgroundColor(getResources().getColor(R.color.white));
            inputNote.setTextColor(getResources().getColor(R.color.blackBackground));
            dialog.dismiss();
        } else if (id == R.id.brownBackground) {
            colorTag = "brown";
            isShowDialog = true;
            onSaveNote();
            inputLayout.setBackgroundColor(getResources().getColor(R.color.brownBackground));
            inputNote.setBackgroundColor(getResources().getColor(R.color.brownBackground));
            inputNote.setTextColor(getResources().getColor(R.color.blackBackground));
            dialog.dismiss();
        } else if (id == R.id.milkyBackground) {
            colorTag = "milky";
            isShowDialog = true;
            onSaveNote();
            inputLayout.setBackgroundColor(getResources().getColor(R.color.milkyBackground));
            inputNote.setBackgroundColor(getResources().getColor(R.color.milkyBackground));
            inputNote.setTextColor(getResources().getColor(R.color.blackBackground));
            dialog.dismiss();
        } else if (id == R.id.greenLightBackground) {
            colorTag = "greenlight";
            isShowDialog = true;
            onSaveNote();
            inputLayout.setBackgroundColor(getResources().getColor(R.color.greenLightBackground));
            inputNote.setBackgroundColor(getResources().getColor(R.color.greenLightBackground));
            inputNote.setTextColor(getResources().getColor(R.color.blackBackground));
            dialog.dismiss();
        } else if (id == R.id.blueBackground) {
            colorTag = "blue";
            isShowDialog = true;
            onSaveNote();
            inputLayout.setBackgroundColor(getResources().getColor(R.color.blueBackground));
            inputNote.setBackgroundColor(getResources().getColor(R.color.blueBackground));
            inputNote.setTextColor(getResources().getColor(R.color.blackBackground));
            dialog.dismiss();
        }
    }

    public void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void hideAttachFileLayout() {
        final Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);

        if (!hidden) {
            mRevealView.startAnimation(slide_down);
            mRevealView.setVisibility(View.GONE);
            hidden = true;
        }
    }

    public void attachFileOnClick(View view) {
        int id = view.getId();
        if (id == R.id.takePhoto) {
            hideAttachFileLayout();
            Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(takePhoto, CAPTURE_PHOTO);

        } else if (id == R.id.choosePhoto) {
            hideAttachFileLayout();
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, SELECT_PHOTO);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 0) {
            if (grantResults.length >= 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == SELECT_PHOTO) {
            if (resultCode == RESULT_OK) {

                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);


                    android.graphics.drawable.BitmapDrawable d = new android.graphics.drawable.BitmapDrawable(getResources(), bitmapResize(selectedImage));
                    SmileyInToEditText(d);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(NotesActivity.this, "خطایی به وجود آمده است لطفا دوباره امتحان کنید", Toast.LENGTH_LONG).show();
                }

            }
        } else if (requestCode == CAPTURE_PHOTO) {
            onCaptureImageResult(data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void onCaptureImageResult(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");
        ImageView imageView = findViewById(R.id.imageView);
        imageView.setImageBitmap(thumbnail);
/*android.graphics.drawable.BitmapDrawable d = new android.graphics.drawable.BitmapDrawable(getResources(),
                getScaledBitmap(thumbnail, inputNote.getWidth(), (int) inputNote.getHeight() / 2));*/

    }

    public void SmileyInToEditText(Drawable drawableHolder) {

        int EditTextCursor;

        SpannableStringBuilder spannableStringBuilder;

        drawableHolder.setBounds(0, 0, drawableHolder.getIntrinsicWidth(), drawableHolder.getIntrinsicHeight());

        EditTextCursor = inputNote.getSelectionEnd();

        inputNote.getText().insert(EditTextCursor, ".");

        EditTextCursor = inputNote.getSelectionStart();

        spannableStringBuilder = new SpannableStringBuilder(inputNote.getText());

        spannableStringBuilder.setSpan(new ImageSpan(drawableHolder),
                EditTextCursor - ".".length(),
                EditTextCursor,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        );

        inputNote.setText(spannableStringBuilder);

        inputNote.setSelection(EditTextCursor);
    }

    public Bitmap bitmapResize(Bitmap imageBitmap) {

        Bitmap bitmap = imageBitmap;
        float heightbmp = bitmap.getHeight();
        float widthbmp = bitmap.getWidth();

        // Get Screen width
        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        float height = heightbmp;
        float width = displaymetrics.widthPixels;

        int convertHeight = (int) width, convertWidth = (int) height;

        // higher
        if (heightbmp > height) {
            convertHeight = (int) height - 20;
            bitmap = Bitmap.createScaledBitmap(bitmap, convertWidth,
                    convertHeight, true);
        }

        // wider
        if (widthbmp > width) {
            convertWidth = (int) width - 20;
            bitmap = Bitmap.createScaledBitmap(bitmap, convertWidth,
                    convertHeight, true);
        }

        return bitmap;
    }

}
