package com.sourcebama.myapplication2.activitys;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.DrawableUtils;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;

import com.sourcebama.myapplication2.App;
import com.sourcebama.myapplication2.R;

import java.util.Locale;

public class SettingsActivity extends AppCompatPreferenceActivity {
    Dialog dialog;
    public static ListPreference lp;
    static String tagKey;
    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.changeTheme(this);
        App.changeFont(this);
        setupActionBar();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            getListView().setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
        }
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MainPreferenceFragment()).commit();
        activity = this;



    }

    public static class MainPreferenceFragment extends PreferenceFragment {

        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);
            getActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

            //theme
            Preference themePref = findPreference(getString(R.string.key_theme));
            themePref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    showAlertDialog(getActivity());
                    return true;
                }
            });

            //font type
            lp = (ListPreference) findPreference(getString(R.string.key_font_type));
            lp.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object value) {
                    // TODO Auto-generated method stub
                    lp.setSummary(lp.getEntries()[Integer.parseInt(value.toString())]);
                    if (lp.getEntries()[Integer.parseInt(value.toString())].equals("نازنین")) {
                        tagKey = "nazanin";
                        App.setFont(tagKey, activity);
                    } else if (lp.getEntries()[Integer.parseInt(value.toString())].equals("ارم")) {
                        tagKey = "aram";
                        App.setFont(tagKey, activity);
                    } else if (lp.getEntries()[Integer.parseInt(value.toString())].equals("ارم2")) {
                        tagKey = "aram2";
                        App.setFont(tagKey, activity);
                    } else if (lp.getEntries()[Integer.parseInt(value.toString())].equals("دانش")) {
                        tagKey = "danesh";
                        App.setFont(tagKey, activity);
                    } else if (lp.getEntries()[Integer.parseInt(value.toString())].equals("فردوسی")) {
                        tagKey = "ferdowsi";
                        App.setFont(tagKey, activity);
                    } else if (lp.getEntries()[Integer.parseInt(value.toString())].equals("حجر")) {
                        tagKey = "hajar";
                        App.setFont(tagKey, activity);
                    } else if (lp.getEntries()[Integer.parseInt(value.toString())].equals("هلا")) {
                        tagKey = "hela";
                        App.setFont(tagKey, activity);
                    } else if (lp.getEntries()[Integer.parseInt(value.toString())].equals("نور")) {
                        tagKey = "noor";
                        App.setFont(tagKey, activity);
                    } else if (lp.getEntries()[Integer.parseInt(value.toString())].equals("پیکسل")) {
                        tagKey = "pixel";
                        App.setFont(tagKey, activity);
                    } else if (lp.getEntries()[Integer.parseInt(value.toString())].equals("تنها")) {
                        tagKey = "tanha";
                        App.setFont(tagKey, activity);
                    } else if (lp.getEntries()[Integer.parseInt(value.toString())].equals("ترافیک")) {
                        tagKey = "trafic";
                        App.setFont(tagKey, activity);
                    } else if (lp.getEntries()[Integer.parseInt(value.toString())].equals("ترافیک2")) {
                        tagKey = "trafic2";
                        App.setFont(tagKey, activity);
                    }
                    App.resetActivity(activity);
                    return true;
                }
            });

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupActionBar() {
        ViewGroup rootView = (ViewGroup) findViewById(R.id.action_bar_root); //id from appcompat
        TextView titleToolbar;
        if (rootView != null) {
            View view = getLayoutInflater().inflate(R.layout.toolbar_layout, rootView, false);
            rootView.addView(view, 0);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("");
            titleToolbar = findViewById(R.id.titleToolbar);
            titleToolbar.setText("تنظیمات");

        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    public void showDialog(Context context) {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_theme);
        dialog.setTitle("تغییر پوسته");
        dialog.show();

    }

    public void setThemeClick(View view) {
        switch (view.getId()) {
            case R.id.layoutOrange:
                App.setTheme("orange", SettingsActivity.this);
                overridePendingTransition(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                App.resetActivity(SettingsActivity.this);
                break;

            case R.id.layoutBlue:
                App.setTheme("blue", SettingsActivity.this);
                overridePendingTransition(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                App.resetActivity(SettingsActivity.this);
                break;

            case R.id.layoutRed:
                App.setTheme("red", SettingsActivity.this);
                overridePendingTransition(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                App.resetActivity(SettingsActivity.this);
                break;
            case R.id.layoutPurple:
                App.setTheme("nothing", SettingsActivity.this);
                overridePendingTransition(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                App.resetActivity(SettingsActivity.this);
                break;
            case R.id.layoutGreen:
                App.setTheme("green", SettingsActivity.this);
                overridePendingTransition(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                App.resetActivity(SettingsActivity.this);
                break;
            case R.id.layoutPink:
                App.setTheme("pink", SettingsActivity.this);
                overridePendingTransition(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                App.resetActivity(SettingsActivity.this);
                break;
            case R.id.layoutBrown:
                App.setTheme("brown", SettingsActivity.this);
                overridePendingTransition(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                App.resetActivity(SettingsActivity.this);
                break;
            case R.id.layoutBlack:
                App.setTheme("black", SettingsActivity.this);
                overridePendingTransition(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                App.resetActivity(SettingsActivity.this);
                break;
            case R.id.layoutNight:
                App.setTheme("night", SettingsActivity.this);
                overridePendingTransition(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                App.resetActivity(SettingsActivity.this);
                break;

        }
    }

    private static void showAlertDialog(Context context) {
        SettingsActivity a = new SettingsActivity();
        a.showDialog(context);

    }


}