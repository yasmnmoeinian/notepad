package com.sourcebama.myapplication2;

import android.content.Context;
import android.content.SharedPreferences;

public class SliderPrefManager {
    private Context context;
    private SharedPreferences preferences;
    private static final String PREF_NAME = "slider_pref";
    private static final String KEY_START = "start_slider";

    public SliderPrefManager(Context  context){
        this.context = context;
        preferences  = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
    }

    public boolean startSlider(){
        return preferences.getBoolean(KEY_START,true);
    }

    public void setStartSlider(boolean start){
        preferences.edit().putBoolean(KEY_START,start).apply();
    }
}
