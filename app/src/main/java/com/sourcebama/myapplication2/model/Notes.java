package com.sourcebama.myapplication2.model;


import androidx.annotation.ColorInt;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "notef")
public class Notes {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "text")
    private String noteText;

    @ColumnInfo(name = "date")
    private long noteDate;

    @ColumnInfo(name = "color")
    private String noteBack;

    @ColumnInfo(name = "title")
    private String noteTitle;

    @Ignore
    private boolean cheched = false;


    public Notes(String noteText, long noteDate, String noteBack, String noteTitle) {
        this.noteText = noteText;
        this.noteDate = noteDate;
        this.noteBack = noteBack;
        this.noteTitle = noteTitle;

    }

    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }

    public long getNoteDate() {
        return noteDate;
    }

    public void setNoteDate(long noteDate) {
        this.noteDate = noteDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNoteBack() {
        return noteBack;
    }

    public void setNoteBack(String noteBack) {
        this.noteBack = noteBack;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public boolean isCheched() {
        return cheched;
    }

    public void setCheched(boolean cheched) {
        this.cheched = cheched;
    }
}
