package com.sourcebama.myapplication2.callback;

import com.sourcebama.myapplication2.model.Notes;

public interface NoteEventListener {

    /**
     *
     * @param notes : note item
     */
    void onNoteClickListener(Notes notes);

    /**
     *
     * @param notes : item
     */
    void onNoteLongClickListener(Notes notes);

}
