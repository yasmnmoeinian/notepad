package com.sourcebama.myapplication2.adapter;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.sourcebama.myapplication2.App;
import com.sourcebama.myapplication2.R;
import com.sourcebama.myapplication2.callback.NoteEventListener;
import com.sourcebama.myapplication2.model.Notes;

import java.util.ArrayList;

import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteHolder> {

    private ArrayList<Notes> notes;
    private NoteEventListener listener;

    public void setListener(NoteEventListener listener) {
        this.listener = listener;
    }

    public NoteAdapter(ArrayList<Notes> notes) {
        this.notes = notes;
    }

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_layout, parent, false);
        return new NoteHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder holder, int position) {

        final Notes note = getNote(position);
        if (note != null) {

            if (note.getNoteTitle().isEmpty()) {
                holder.noteText.setText(note.getNoteText());
            } else {
                holder.noteText.setText(note.getNoteTitle());
            }

            String backColor = note.getNoteBack();

            App.changeBackground(holder.linearLayout, backColor);

            PersianDate pdate = new PersianDate();
            PersianDateFormat pdformater1 = new PersianDateFormat("j F");
            holder.noteDate.setText(pdformater1.format(pdate));

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onNoteClickListener(note);
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listener.onNoteLongClickListener(note);
                    return false;
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return notes.size();
    }

    private Notes getNote(int position) {
        return notes.get(position);
    }


    class NoteHolder extends RecyclerView.ViewHolder {
        TextView noteText, noteDate;
        LinearLayout linearLayout;

        public NoteHolder(View itemView) {
            super(itemView);
            noteText = itemView.findViewById(R.id.noteText);
            noteDate = itemView.findViewById(R.id.noteDate);
            linearLayout = itemView.findViewById(R.id.linearNotes);

        }

    }

}
