package com.sourcebama.myapplication2;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Handler;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.google.android.material.textfield.TextInputLayout;
import com.sourcebama.myapplication2.activitys.SettingsActivity;
import com.sourcebama.myapplication2.model.Notes;

import java.util.ArrayList;

public class App extends Application {
    public static Context context;
    public static AppCompatActivity activity;
    public static final Handler HANDLER = new Handler();
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;
    private static String THEME_KEY = "currentTheme";
    private static String FONT_KEY = "currentFont";

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static void startActivity(Class activity, boolean finish) {
        Intent intent = new Intent(App.activity, activity);
        App.activity.startActivity(intent);
        if (finish) {
            App.activity.finish();
        }
    }

    public static void setTheme(String key, SettingsActivity activity) {
        preferences = activity.getSharedPreferences("ThemePref", MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(THEME_KEY, key);
        editor.apply();

    }

    public static void resetActivity(Activity activity) {
     /*  activity.finish();
        activity.startActivity(activity.getIntent());*/
        Intent i = activity.getBaseContext().getPackageManager().getLaunchIntentForPackage(activity.getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(i);
        activity.finish();
    }

    public static void changeThemeLinear(LinearLayout linearLayout) {
        preferences = activity.getSharedPreferences("ThemePref", Context.MODE_PRIVATE);
        if (preferences.getString(THEME_KEY, "nothing").equals("blue")) {
            linearLayout.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDarkBlue));


        } else if (preferences.getString(THEME_KEY, "nothing").equals("red")) {
            linearLayout.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDarkRed));


        } else if (preferences.getString(THEME_KEY, "nothing").equals("orange")) {
            linearLayout.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDarkOrange));


        } else if (preferences.getString(THEME_KEY, "nothing").equals("pink")) {
            linearLayout.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDarkPink));


        } else if (preferences.getString(THEME_KEY, "nothing").equals("green")) {
            linearLayout.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDarkGreen));

        } else if (preferences.getString(THEME_KEY, "nothing").equals("brown")) {
            linearLayout.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDarkBrown));

        } else if (preferences.getString(THEME_KEY, "nothing").equals("black")) {
            linearLayout.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDarkBlack));

        } else if (preferences.getString(THEME_KEY, "nothing").equals("night")) {
            linearLayout.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDarkNight));

        } else {
            linearLayout.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));

        }
    }

    public static void changeTheme(Activity activity) {
        preferences = activity.getSharedPreferences("ThemePref", Context.MODE_PRIVATE);
        if (preferences.getString(THEME_KEY, "nothing").equals("blue")) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            activity.getTheme().applyStyle(R.style.ThemeBlue, true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDarkBlue));
            }

        } else if (preferences.getString(THEME_KEY, "nothing").equals("red")) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

            activity.getTheme().applyStyle(R.style.ThemeRed, true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDarkRed));
            }

        } else if (preferences.getString(THEME_KEY, "nothing").equals("orange")) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

            activity.getTheme().applyStyle(R.style.ThemeOrange, true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDarkOrange));
            }

        } else if (preferences.getString(THEME_KEY, "nothing").equals("pink")) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

            activity.getTheme().applyStyle(R.style.ThemePink, true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDarkPink));
            }

        } else if (preferences.getString(THEME_KEY, "nothing").equals("green")) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

            activity.getTheme().applyStyle(R.style.ThemeGreen, true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDarkGreen));
            }

        } else if (preferences.getString(THEME_KEY, "nothing").equals("brown")) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

            activity.getTheme().applyStyle(R.style.ThemeBrown, true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDarkBrown));
            }

        } else if (preferences.getString(THEME_KEY, "nothing").equals("black")) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

            activity.getTheme().applyStyle(R.style.ThemeBlack, true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDarkBlack));
            }

        } else if (preferences.getString(THEME_KEY, "nothing").equals("night")) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

            activity.getTheme().applyStyle(R.style.NightMode, true);
            activity.getWindow().setBackgroundDrawableResource(R.drawable.back_night_mode);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDarkNight));
            }

        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            activity.getTheme().applyStyle(R.style.AppTheme, true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDark));
            }

        }
    }

    public static void setBackgroundNote(Notes temp, TextView inputNote, TextInputLayout inputLayout) {
        if (temp.getNoteBack().equals("black")) {

            inputLayout.setBackgroundColor(activity.getResources().getColor(R.color.blackBackground));
            inputNote.setBackgroundColor(activity.getResources().getColor(R.color.blackBackground));
            inputNote.setTextColor(activity.getResources().getColor(R.color.colorAccentNight));

        } else if (temp.getNoteBack().equals("gray")) {

            inputLayout.setBackgroundColor(activity.getResources().getColor(R.color.grayBackground));
            inputNote.setBackgroundColor(activity.getResources().getColor(R.color.grayBackground));
            inputNote.setTextColor(activity.getResources().getColor(R.color.blackBackground));

        } else if (temp.getNoteBack().equals("pink")) {

            inputLayout.setBackgroundColor(activity.getResources().getColor(R.color.pinkBackground));
            inputNote.setBackgroundColor(activity.getResources().getColor(R.color.pinkBackground));
            inputNote.setTextColor(activity.getResources().getColor(R.color.blackBackground));

        } else if (temp.getNoteBack().equals("orange")) {

            inputLayout.setBackgroundColor(activity.getResources().getColor(R.color.orangeBackground));
            inputNote.setBackgroundColor(activity.getResources().getColor(R.color.orangeBackground));
            inputNote.setTextColor(activity.getResources().getColor(R.color.blackBackground));

        } else if (temp.getNoteBack().equals("yellow")) {

            inputLayout.setBackgroundColor(activity.getResources().getColor(R.color.yellowBackground));
            inputNote.setBackgroundColor(activity.getResources().getColor(R.color.yellowBackground));
            inputNote.setTextColor(activity.getResources().getColor(R.color.blackBackground));

        } else if (temp.getNoteBack().equals("green")) {

            inputLayout.setBackgroundColor(activity.getResources().getColor(R.color.greenBackground));
            inputNote.setBackgroundColor(activity.getResources().getColor(R.color.greenBackground));
            inputNote.setTextColor(activity.getResources().getColor(R.color.blackBackground));

        } else if (temp.getNoteBack().equals("purple")) {

            inputLayout.setBackgroundColor(activity.getResources().getColor(R.color.purpleBackground));
            inputNote.setBackgroundColor(activity.getResources().getColor(R.color.purpleBackground));
            inputNote.setTextColor(activity.getResources().getColor(R.color.blackBackground));

        } else if (temp.getNoteBack().equals("white")) {

            inputLayout.setBackgroundColor(activity.getResources().getColor(R.color.white));
            inputNote.setBackgroundColor(activity.getResources().getColor(R.color.white));
            inputNote.setTextColor(activity.getResources().getColor(R.color.blackBackground));

        } else if (temp.getNoteBack().equals("brown")) {

            inputLayout.setBackgroundColor(activity.getResources().getColor(R.color.brownBackground));
            inputNote.setBackgroundColor(activity.getResources().getColor(R.color.brownBackground));
            inputNote.setTextColor(activity.getResources().getColor(R.color.blackBackground));

        } else if (temp.getNoteBack().equals("milky")) {

            inputLayout.setBackgroundColor(activity.getResources().getColor(R.color.milkyBackground));
            inputNote.setBackgroundColor(activity.getResources().getColor(R.color.milkyBackground));
            inputNote.setTextColor(activity.getResources().getColor(R.color.blackBackground));

        } else if (temp.getNoteBack().equals("greenlight")) {

            inputLayout.setBackgroundColor(activity.getResources().getColor(R.color.greenLightBackground));
            inputNote.setBackgroundColor(activity.getResources().getColor(R.color.greenLightBackground));
            inputNote.setTextColor(activity.getResources().getColor(R.color.blackBackground));

        } else if (temp.getNoteBack().equals("blue")) {

            inputLayout.setBackgroundColor(activity.getResources().getColor(R.color.blueBackground));
            inputNote.setBackgroundColor(activity.getResources().getColor(R.color.blueBackground));
            inputNote.setTextColor(activity.getResources().getColor(R.color.blackBackground));

        }
    }

    public static void setFont(String key, Activity activity) {
        preferences = activity.getSharedPreferences("FontPref", MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(FONT_KEY, key);
        editor.apply();
    }

    public static void changeFont(Activity activity) {
        preferences = activity.getSharedPreferences("FontPref", Context.MODE_PRIVATE);

        if (preferences.getString(FONT_KEY, "nazanin").equals("nazanin")) {
            activity.getTheme().applyStyle(R.style.bNazanin, true);

        } else if (preferences.getString(FONT_KEY, "nazanin").equals("aram2")) {
            activity.getTheme().applyStyle(R.style.aramBold, true);

        } else if (preferences.getString(FONT_KEY, "nazanin").equals("aram")) {
            activity.getTheme().applyStyle(R.style.aram, true);

        } else if (preferences.getString(FONT_KEY, "nazanin").equals("danesh")) {
            activity.getTheme().applyStyle(R.style.danesh, true);

        } else if (preferences.getString(FONT_KEY, "nazanin").equals("ferdowsi")) {
            activity.getTheme().applyStyle(R.style.ferdowsi, true);

        } else if (preferences.getString(FONT_KEY, "nazanin").equals("hajar")) {
            activity.getTheme().applyStyle(R.style.hajar, true);

        } else if (preferences.getString(FONT_KEY, "nazanin").equals("hela")) {
            activity.getTheme().applyStyle(R.style.hela, true);

        } else if (preferences.getString(FONT_KEY, "nazanin").equals("noor")) {
            activity.getTheme().applyStyle(R.style.noor, true);

        } else if (preferences.getString(FONT_KEY, "nazanin").equals("pixel")) {
            activity.getTheme().applyStyle(R.style.pixel, true);

        } else if (preferences.getString(FONT_KEY, "nazanin").equals("tanha")) {
            activity.getTheme().applyStyle(R.style.tanha, true);

        } else if (preferences.getString(FONT_KEY, "nazanin").equals("trafic")) {
            activity.getTheme().applyStyle(R.style.trafiic, true);

        } else if (preferences.getString(FONT_KEY, "nazanin").equals("trafic2")) {
            activity.getTheme().applyStyle(R.style.trafiicBold, true);

        } else {
            activity.getTheme().applyStyle(R.style.bNazanin, true);
        }
    }

    public static  void changeBackground(LinearLayout linearLayout, String backColor) {
        if (backColor.equals("black")) {
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.blackBackground));
        } else if (backColor.equals("gray")) {
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.grayBackground));


        } else if (backColor.equals("pink")) {
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.pinkBackground));

        } else if (backColor.equals("orange")) {
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.orangeBackground));


        } else if (backColor.equals("yellow")) {
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.yellowBackground));


        } else if (backColor.equals("green")) {
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.greenBackground));



        } else if (backColor.equals("purple")) {
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.purpleBackground));



        } else if (backColor.equals("white")) {
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.white));



        } else if (backColor.equals("brown")) {
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.brownBackground));



        } else if (backColor.equals("milky")) {
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.milkyBackground));



        } else if (backColor.equals("greenlight")) {
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.greenLightBackground));



        } else if (backColor.equals("blue")) {
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.blueBackground));


        }
    }
}