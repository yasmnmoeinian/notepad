package com.sourcebama.myapplication2;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ActivityEnhanced extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.activity = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.activity = this;
    }
}
